require 'yaml'
require 'fileutils'

class Config

    attr_accessor :content

    def initialize(path, filename, content = nil)
      raise 'You must specify a path for your config file' if path.nil?
      raise 'You must specify a filename for your config file.' if filename.nil?
      @filename = filename
      @filename += '.yaml' unless @filename.include? '.yaml' || @filename.include? '.yml'
      @full_path = path + @filename
      # Create the path if it's not there
      FileUtils.mkdir_p(path) unless Dir.exist?(path)
      if content.nil?
        FileUtils.touch(@full_path)
      else
        raise 'Content must be a hash' unless content.is_a? Hash
        @content = content
        unless File.file?(@full_path) do
          File.open(@full_path, 'w') do |file|
            file.write(content.to_yaml)
          end
        end
      end
    end

    def load
      return YAML.load(File.read(@full_path))
    end

    def save_file
      unless File.file?(@full_path) do
        File.open(@full_path, 'w') do |file|
          file.write(@content.to_yaml)
        end
      end
    end
end